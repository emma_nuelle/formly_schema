import { FormlySchemaPage } from './app.po';

describe('formly-schema App', () => {
  let page: FormlySchemaPage;

  beforeEach(() => {
    page = new FormlySchemaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
