import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import SurveySurvey from './survey';

@Injectable()
export default class SurveySurveyServiceBase {
  private baseUrl: string = '/api/v1/survey/surveys/';

  constructor(private http : Http){
  }

  query(search: any): Observable<SurveySurvey[]> {
    const records$ = this.http
      .get(this.baseUrl, {search, headers: this.getHeaders()})
      .map(mapRecords)
      .catch(handleError);
    return records$;
  }

  getAll(): Observable<SurveySurvey[]> {
    return this.query('');
  }

  get(id: string): Observable<SurveySurvey> {
    const record$ = this.http
      .get(`${this.baseUrl}${id}/`, {headers: this.getHeaders()})
      .map(mapRecord)
      .catch(handleError);
    return record$;
  }

  create(record: SurveySurvey) : Observable<SurveySurvey>{
    const record$ = this.http
      .post(this.baseUrl, JSON.stringify(record), {headers: this.getHeaders()})
      .map(mapRecord)
      .catch(handleError);
    return record$;
  }

  update(record: SurveySurvey) : Observable<SurveySurvey>{
    const record$ = this.http
      .put(`${this.baseUrl}${record.id}/`, JSON.stringify(record), {headers: this.getHeaders()})
      .map(mapRecord)
      .catch(handleError);
    return record$;
  }

  delete(record: SurveySurvey) : Observable<Response>{
    return this.http
      .delete(`${this.baseUrl}${record.id}/`, {headers: this.getHeaders()});
  }

  metadata() : Observable<any> {
    const data$ = this.http
    .options(this.baseUrl, {headers: this.getHeaders()})
    .map((response) => {
      return response.json();
    })
    .catch(handleError);
    return data$;
  }

  private getHeaders() {
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('content-type', 'application/json');
    return headers;
  }
}

function mapRecords(response:Response): SurveySurvey[] {
  return response.json().results.map((json:any): SurveySurvey => {
    return new SurveySurvey(json);
  });
}

function mapRecord(response:Response): SurveySurvey {
  return new SurveySurvey(response.json());
}

function handleError(error: any) {
  console.error(error);
  return Observable.throw(error.message || 'Error while fetching survey/survey');
}
