export default class SurveySurveyBase {
  id:string;
  
    full_name:string;
  
    country:string;
  
    city:string;
  
    i_use_flask:boolean;
  
    i_use_django:boolean;
  
    i_use_pyramid:boolean;
  
    i_use_ruby_on_rails:boolean;
  
    i_use_sinatra:boolean;
  
    i_use_phoenix:boolean;
  
    i_use_cake_php:boolean;
  
    i_use_symfony:boolean;
  
    i_use_spray:boolean;
  
    i_use_play:boolean;
  
    i_use_jquery:boolean;
  
    i_use_ember:boolean;
  
    i_use_angular1:boolean;
  
    i_use_angular2:boolean;
  
    i_use_react:boolean;
  
    i_use_alexa:boolean;
  
    i_use_vue:boolean;
  
    my_favourite_language_is:string;
  
    favourite_language_other:string;
  
    feedback:string;
  
    __str__:string;
  
  

  constructor(json:any) {
    if (json) {
      this.id = json.id;
      
        this.full_name = json.full_name;
      
        this.country = json.country;
      
        this.city = json.city;
      
        this.i_use_flask = json.i_use_flask;
      
        this.i_use_django = json.i_use_django;
      
        this.i_use_pyramid = json.i_use_pyramid;
      
        this.i_use_ruby_on_rails = json.i_use_ruby_on_rails;
      
        this.i_use_sinatra = json.i_use_sinatra;
      
        this.i_use_phoenix = json.i_use_phoenix;
      
        this.i_use_cake_php = json.i_use_cake_php;
      
        this.i_use_symfony = json.i_use_symfony;
      
        this.i_use_spray = json.i_use_spray;
      
        this.i_use_play = json.i_use_play;
      
        this.i_use_jquery = json.i_use_jquery;
      
        this.i_use_ember = json.i_use_ember;
      
        this.i_use_angular1 = json.i_use_angular1;
      
        this.i_use_angular2 = json.i_use_angular2;
      
        this.i_use_react = json.i_use_react;
      
        this.i_use_alexa = json.i_use_alexa;
      
        this.i_use_vue = json.i_use_vue;
      
        this.my_favourite_language_is = json.my_favourite_language_is;
      
        this.favourite_language_other = json.favourite_language_other;
      
        this.feedback = json.feedback;
      
        this.__str__ = json.__str__;
      
      
    }
  }
}
