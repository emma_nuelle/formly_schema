import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import TodoTask from './task';

@Injectable()
export default class TodoTaskServiceBase {
  private baseUrl: string = '/api/v1/todo/tasks/';

  constructor(private http : Http){
  }

  query(search: any): Observable<TodoTask[]> {
    const records$ = this.http
      .get(this.baseUrl, {search, headers: this.getHeaders()})
      .map(mapRecords)
      .catch(handleError);
    return records$;
  }

  getAll(): Observable<TodoTask[]> {
    return this.query('');
  }

  get(id: string): Observable<TodoTask> {
    const record$ = this.http
      .get(`${this.baseUrl}${id}/`, {headers: this.getHeaders()})
      .map(mapRecord)
      .catch(handleError);
    return record$;
  }

  create(record: TodoTask) : Observable<TodoTask>{
    const record$ = this.http
      .post(this.baseUrl, JSON.stringify(record), {headers: this.getHeaders()})
      .map(mapRecord)
      .catch(handleError);
    return record$;
  }

  update(record: TodoTask) : Observable<TodoTask>{
    const record$ = this.http
      .put(`${this.baseUrl}${record.id}/`, JSON.stringify(record), {headers: this.getHeaders()})
      .map(mapRecord)
      .catch(handleError);
    return record$;
  }

  delete(record: TodoTask) : Observable<Response>{
    return this.http
      .delete(`${this.baseUrl}${record.id}/`, {headers: this.getHeaders()});
  }

  metadata() : Observable<any> {
    const data$ = this.http
    .options(this.baseUrl, {headers: this.getHeaders()})
    .map((response) => {
      return response.json();
    })
    .catch(handleError);
    return data$;
  }

  private getHeaders() {
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('content-type', 'application/json');
    return headers;
  }
}

function mapRecords(response:Response): TodoTask[] {
  return response.json().results.map((json:any): TodoTask => {
    return new TodoTask(json);
  });
}

function mapRecord(response:Response): TodoTask {
  return new TodoTask(response.json());
}

function handleError(error: any) {
  console.error(error);
  return Observable.throw(error.message || 'Error while fetching todo/task');
}
