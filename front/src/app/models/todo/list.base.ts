export default class TodoListBase {
  id:string;
  
    name:string;
  
    __str__:string;
  
  

  constructor(json:any) {
    if (json) {
      this.id = json.id;
      
        this.name = json.name;
      
        this.__str__ = json.__str__;
      
      
    }
  }
}
