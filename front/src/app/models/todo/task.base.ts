export default class TodoTaskBase {
  id:string;
  
    description:string;
  
    done:boolean;
  
    __str__:string;
  
  
    lst:string;
  

  constructor(json:any) {
    if (json) {
      this.id = json.id;
      
        this.description = json.description;
      
        this.done = json.done;
      
        this.__str__ = json.__str__;
      
      
        this.lst = json.lst;
      
    }
  }
}
