import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import TodoList from './list';

@Injectable()
export default class TodoListServiceBase {
  private baseUrl: string = '/api/v1/todo/lists/';

  constructor(private http : Http){
  }

  query(search: any): Observable<TodoList[]> {
    const records$ = this.http
      .get(this.baseUrl, {search, headers: this.getHeaders()})
      .map(mapRecords)
      .catch(handleError);
    return records$;
  }

  getAll(): Observable<TodoList[]> {
    return this.query('');
  }

  get(id: string): Observable<TodoList> {
    const record$ = this.http
      .get(`${this.baseUrl}${id}/`, {headers: this.getHeaders()})
      .map(mapRecord)
      .catch(handleError);
    return record$;
  }

  create(record: TodoList) : Observable<TodoList>{
    const record$ = this.http
      .post(this.baseUrl, JSON.stringify(record), {headers: this.getHeaders()})
      .map(mapRecord)
      .catch(handleError);
    return record$;
  }

  update(record: TodoList) : Observable<TodoList>{
    const record$ = this.http
      .put(`${this.baseUrl}${record.id}/`, JSON.stringify(record), {headers: this.getHeaders()})
      .map(mapRecord)
      .catch(handleError);
    return record$;
  }

  delete(record: TodoList) : Observable<Response>{
    return this.http
      .delete(`${this.baseUrl}${record.id}/`, {headers: this.getHeaders()});
  }

  metadata() : Observable<any> {
    const data$ = this.http
    .options(this.baseUrl, {headers: this.getHeaders()})
    .map((response) => {
      return response.json();
    })
    .catch(handleError);
    return data$;
  }

  private getHeaders() {
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('content-type', 'application/json');
    return headers;
  }
}

function mapRecords(response:Response): TodoList[] {
  return response.json().results.map((json:any): TodoList => {
    return new TodoList(json);
  });
}

function mapRecord(response:Response): TodoList {
  return new TodoList(response.json());
}

function handleError(error: any) {
  console.error(error);
  return Observable.throw(error.message || 'Error while fetching todo/list');
}
