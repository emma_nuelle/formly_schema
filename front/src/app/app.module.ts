import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormlyModule, FormlyBootstrapModule } from 'ng2-formly';
import { HttpModule, XSRFStrategy, CookieXSRFStrategy } from '@angular/http';

import { AppComponent } from './app.component';
import { TodoListsComponent } from './components/todo-lists/todo-lists.component';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoTaskComponent } from './components/todo-task/todo-task.component';
import { SurveyComponent } from './components/survey/survey.component';


export class CSRFCookie extends CookieXSRFStrategy {
  constructor() {
    super('csrftoken', 'X-CSRFToken');
  }
}


@NgModule({
  declarations: [
    AppComponent,
    TodoListsComponent,
    TodoListComponent,
    TodoTaskComponent,
    SurveyComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    FormlyModule.forRoot(),
    FormlyBootstrapModule,
    HttpModule
  ],
  providers: [
    {
      provide: XSRFStrategy,
      useClass: CSRFCookie
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

// vim: backupcopy=yes
