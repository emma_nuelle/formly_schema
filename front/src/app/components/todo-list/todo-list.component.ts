import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import TodoTaskService from '../../models/todo/task.service';
import TodoTask from '../../models/todo/task';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss'],
  providers: [TodoTaskService]
})
export class TodoListComponent implements OnInit {

  @Input()
  list;
  tasks: TodoTask[];
  newTask: string = '';
  errorMessage: string;

  constructor(private todoTaskService:TodoTaskService) { }

  ngOnInit() {
    this.todoTaskService
      .query(`lst_id=${this.list.id}`)
      .subscribe(
        result => this.tasks = result,
        error => this.errorMessage = error
      );
  }

  clearCompleted() {
    const completedTasks = this.tasks.filter((task) => {
      return task.done;
    });
    const observables = [];
    completedTasks.forEach((task) => {
      observables.push(this.todoTaskService.delete(task));
    });
    Observable.forkJoin(observables).subscribe(
      (r) => {
        this.todoTaskService
          .query(`lst_id=${this.list.id}`)
          .subscribe(
            result => this.tasks = result,
            error => this.errorMessage = error
          );
      }
    );
  }

  addTask(description: string) {
    const task = new TodoTask({description, lst: this.list.id});
    this.todoTaskService.create(task).subscribe(
      result =>  {
        this.tasks.push(result);
        this.newTask = '';
      },
      error => this.errorMessage = error
    );
  }

}

// vim: backupcopy=yes
