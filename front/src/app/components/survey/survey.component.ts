import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from 'ng2-formly';
import SurveySurveyService from '../../models/survey/survey.service';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss'],
  providers: [SurveySurveyService],
})
export class SurveyComponent implements OnInit {

  form: FormGroup = new FormGroup({});
  formFields: Array<FormlyFieldConfig>;
  data = {};
  errorMessage: string;
  filled = false;

  constructor(private surveySurveyService: SurveySurveyService) { }

  ngOnInit() {
    this.surveySurveyService
      .metadata()
      .subscribe(
        result => this.formFields = result,
        error => this.errorMessage = error
      );
  }

  submit(data) {
    console.log(data);
    this.surveySurveyService
      .create(data)
      .subscribe(
        result => this.filled = true,
        error => console.error(error)
      );
  }
}

// vim: backupcopy=yes
