import { Component, OnInit } from '@angular/core';
import TodoListService from '../../models/todo/list.service';
import TodoList from '../../models/todo/list';

@Component({
  selector: 'app-todo-lists',
  templateUrl: './todo-lists.component.html',
  styleUrls: ['./todo-lists.component.scss'],
  providers: [TodoListService],
})
export class TodoListsComponent implements OnInit {

  lists: TodoList[];
  newList:string = '';
  errorMessage: string;

  constructor(private todoListService : TodoListService) { }

  ngOnInit() {
    this.todoListService
      .getAll()
      .subscribe(
        result => this.lists = result,
        error => this.errorMessage = error
      );
  }

  addList(name: string) {
    const list = new TodoList({name});
    this.todoListService.create(list).subscribe(
      result => {
        this.lists.push(result);
        this.newList = '';
      },
      error => this.errorMessage = error
    )
  }

}

// vim: backupcopy=yes
