import { Component, Input } from '@angular/core';
import TodoTaskService from '../../models/todo/task.service';

@Component({
  selector: 'app-todo-task',
  templateUrl: './todo-task.component.html',
  styleUrls: ['./todo-task.component.scss'],
  providers: [TodoTaskService]
})
export class TodoTaskComponent {

  @Input()
  task;
  errorMessage: string;

  constructor(private todoTaskService: TodoTaskService) { }

  toggle() {
    this.task.done = !this.task.done;
    this.todoTaskService.update(this.task).subscribe(
      result => { },
      error => {
        this.errorMessage = error;
        this.task.done = !this.task.done;
      }
    );
  }

}

// vim: backupcopy=yes
