# Formly-schema-adapter

## Backend Django:

In the `back` directory, run `./run.sh`

## Fronten Angular2

:warning: requirements: you need to install angular-cli globally first `sudo npm install -g @angular/cli`

In the `front` directory run `./run.sh`

License information available [here](LICENSE.md).

Contributors code of conduct is available [here](COC.md). Not that this COC **will** be enforced.
