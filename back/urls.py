from django.conf.urls import include, url
from django.contrib import admin

from rest_framework import urls as drf_urls

from formly_schema import api_urls


urlpatterns = [
    # Examples:
    # url(r'^$', 'formly-schema-adapter.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/v1/', include(api_urls)),
    url(r'^api-auth', include(drf_urls)),
]
