from drf_auto_endpoint.endpoints import Endpoint
from drf_auto_endpoint.router import register

from rest_framework.permissions import AllowAny

from .models import Survey


@register
class SurveyEndpoint(Endpoint):
    model = Survey
    permission_classes = (AllowAny, )
    fieldsets = [
        'full_name',
        {
            'type': 'fieldset',
            'fields': [
                {
                    'key': 'country',
                    'className': 'col-sm-6',
                }, {
                    'key': 'city',
                    'className': 'col-sm-6',
                },
            ],
        }, {
            'type': 'fieldset',
            'label': 'On the backend...',
            'className': 'col-sm-6',
            'fields': [
                'i_use_flask',
                'i_use_django',
                'i_use_pyramid',
                'i_use_ruby_on_rails',
                'i_use_sinatra',
                'i_use_phoenix',
                'i_use_cake_php',
                'i_use_symfony',
                'i_use_spray',
                'i_use_play',
            ]
        }, {
            'type': 'fieldset',
            'className': 'col-sm-6',
            'label': 'On the frontend...',
            'fields': [
                'i_use_jquery',
                'i_use_ember',
                'i_use_angular1',
                'i_use_angular2',
                'i_use_react',
                'i_use_alexa',
                'i_use_vue',
            ]
        }, {
            "className": "section-label clearfix",
            "template": "<hr />",
        },
        'my_favourite_language_is',
        {
            'key': 'favourite_language_other',
            'hideExpression': 'model.my_favourite_language_is!=="other"',
            'templateOptions': {
                'placeholder': 'your favourite labguage',
                'label': None,
            }
        }, {
            "className": "section-label",
            "template": "<hr />",
        }, {
            'key': 'feedback',
            'type': 'textarea',
            'templateOptions': {
                'rows': 10,
            }
        }
    ]

