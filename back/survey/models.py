from django.db import models


class Survey(models.Model):

    LANGUAGE_CHOICES = (
        'python',
        'php',
        'ruby',
        'elixir',
        'go',
        'elm',
        'javascript',
        'other',
    )

    full_name = models.CharField(max_length=255)

    country = models.CharField(max_length=100)
    city = models.CharField(max_length=100)

    i_use_flask = models.BooleanField(default=False)
    i_use_django = models.BooleanField(default=False)
    i_use_pyramid = models.BooleanField(default=False)
    i_use_ruby_on_rails = models.BooleanField(default=False)
    i_use_sinatra = models.BooleanField(default=False)
    i_use_phoenix = models.BooleanField(default=False)
    i_use_cake_php = models.BooleanField(default=False)
    i_use_symfony = models.BooleanField(default=False)
    i_use_spray = models.BooleanField(default=False)
    i_use_play = models.BooleanField(default=False)

    i_use_jquery = models.BooleanField(default=False)
    i_use_ember = models.BooleanField(default=False)
    i_use_angular1 = models.BooleanField(default=False)
    i_use_angular2 = models.BooleanField(default=False)
    i_use_react = models.BooleanField(default=False)
    i_use_alexa = models.BooleanField(default=False)
    i_use_vue = models.BooleanField(default=False)

    my_favourite_language_is = models.CharField(max_length=50, choices=((v, v) for v in LANGUAGE_CHOICES))
    favourite_language_other = models.CharField(max_length=50, null=True, blank=True)

    feedback = models.TextField(null=True, blank=True)

    @property
    def favourite_langue(self):
        if self.my_favourite_language_is != 'other':
            return self.my_favourite_language_is
        return self.favourite_language_other
