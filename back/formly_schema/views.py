from django.contrib.auth import get_user_model

from rest_framework.viewsets import ModelViewSet
from rest_framework import filters

from .serializers import UserSerializer


class UserViewSet(ModelViewSet):

    serializer_class = UserSerializer
    queryset = get_user_model().objects.all()
    filter_backends = (filters.DjangoFilterBackend, )
    filter_fields = ('first_name', 'last_name',)

