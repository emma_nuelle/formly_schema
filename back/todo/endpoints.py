from drf_auto_endpoint.endpoints import Endpoint
from drf_auto_endpoint.router import register

from rest_framework.permissions import AllowAny

from .models import List, Task


@register
class TaskEndpoint(Endpoint):

    model = Task
    permission_classes = (AllowAny, )
    filter_fields = ('done', 'lst_id')
    fieldsets = [{'key': 'description', }, ]
    fields_annotation = {'description': {'placeholder': 'Add a new task'}}


@register
class ListEndpoint(Endpoint):

    model = List
    permission_classes = (AllowAny, )
    fieldsets = [{'key': 'name', }, ]
    fields_annotation = {'name': {'placeholder': 'Create a new list'}}
